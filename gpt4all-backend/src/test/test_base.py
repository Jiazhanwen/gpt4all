import unittest
from gpt4all import GPT4All


class TestBase(unittest.TestCase):
    def test_base(self):
        model = GPT4All(model_name="ggml-all-MiniLM-L6-v2-f16.bin", model_path="/opt/gpt4all/model/", allow_download=False)
        print('==' + model.generate(prompt="stupid", max_tokens=20))
