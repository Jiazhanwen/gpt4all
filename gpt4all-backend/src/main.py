import time

import flask
from gpt4all import GPT4All
from flask import Flask

app = Flask(__name__)

root_path = '/service'
model_path = "/opt/gpt4all/model/"
model = GPT4All(model_name="ggml-all-MiniLM-L6-v2-f16.bin", model_path=model_path, allow_download=False)


@app.route(root_path + '/get_model', methods=['GET'])
def get_model():
    return {"models": []}


@app.route(root_path + '/quest', methods=['GET'])
def test_base():
    def gen():
        for token in model.generate("Who are you", max_tokens=20, streaming=True):
            yield f'data: {token}\n\n'
        yield 'event: finish\ndata: finish.\n\n'

    return flask.Response(gen(), mimetype="text/event-stream")


if __name__ == '__main__':
    app.debug = True
    app.run()
