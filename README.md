## GPT4ALL-BACKEND
### 容器准备
1. 拉取镜像
2. 复制模型文件到宿主机模型目录/opt/gpt4all/model/
### 镜像制作
1. 复制src目录到Dockerfile平级目录
2. `docker build . -t gpt4all:<version>`
3. push镜像
### 单容器启动
```shell
docker run -d -v /opt/gpt4all/model/:/opt/gpt4all/model -p 8084:8084 gpt4all:<version>
```
### 访问方式
以http rest方式提供服务
